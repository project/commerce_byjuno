<?php

namespace Drupal\commerce_byjuno\PluginForm;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for adding Byjuno payment methods.
 *
 * @package Drupal\commerce_byjuno\PluginForm
 */
class ByjunoPaymentMethodAddForm extends PaymentMethodAddForm implements TrustedCallbackInterface {

  /**
   * The order.
   * @TODO Remove if the order gets added to the base PaymentMethodAddForm
   *   See: https://www.drupal.org/project/commerce/issues/3112812
   *
   * @var \Drupal\commerce_order\entity\OrderInterface
   */
  protected $order = NULL;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $date_formatter;

  /**
   * Constructs a new PaymentMethodAddForm.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_fomatter
   */
  public function __construct(CurrentStoreInterface $current_store, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager, LoggerInterface $logger, RouteMatchInterface $route_match, DateFormatterInterface $date_fomatter) {
      parent::__construct($current_store, $entity_type_manager, $inline_form_manager, $logger);

      // Set the order, if available, for ThreatMatrix.
      if ($order = $route_match->getParameter('commerce_order')) {
          $this->order = $order;
        }

      $this->date_formatter = $date_fomatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
      return new static(
          $container->get('commerce_store.current_store'),
          $container->get('entity_type.manager'),
          $container->get('plugin.manager.commerce_inline_form'),
          $container->get('logger.channel.commerce_payment'),
          $container->get('current_route_match'),
          $container->get('date.formatter')
        );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_gateway_configuration = $payment_method->getPaymentGateway()->getPluginConfiguration();
    $payment_method_options = $payment_method->getPaymentGateway()->getPlugin()->getPaymentMethodOptions();

    if (!empty($this->order)) {
      $org_id = 'lq866c5i'; // This is the Byjuno Organisation ID
      $session_id = $this->order->uuid();
      $page_id = $payment_gateway_configuration['client_id'];

      $form['payment_details']['threatmetrix']['script'] = [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#attributes' => [
          'src' => "https://h.online-metrix.net/tags.js?org_id=$org_id&session_id=$session_id&pageid=$page_id",
          'type'=> "text/javascript",
        ],
      ];

      $form['payment_details']['threatmetrix']['noscript'] = [
        '#type' => 'html_tag',
        '#tag' => 'iframe',
        '#noscript' => TRUE,
        '#attributes' => [
          'src' => "https://h.online-metrix.net/tags?org_id=$org_id&session_id=$session_id&pageid=$page_id"
        ],
      ];
    }

    $form['payment_details']['#title'] = $this->t('Credit check details');

    $form['payment_details']['information_message'] = [
      '#markup' => $payment_gateway_configuration['information_message']['value'],
    ];

    $customer_type_options = [];
    if (!empty($payment_gateway_configuration['person_payment_methods'])) {
      $customer_type_options['person'] = $this->t('Private Individual');
    }
    if (!empty($payment_gateway_configuration['company_payment_methods'])) {
      $customer_type_options['company'] = $this->t('Company/Organisation');
    }
    if (empty($customer_type_options)) {
      throw new PaymentGatewayException('At least one payment option must be enabled');
    }

    $form['payment_details']['byjuno_customer_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Please select:'),
      '#required' => TRUE,
      '#options' => $customer_type_options,
    ];

    $form['payment_details']['byjuno_gender'] = [
      '#type' => 'radios',
      '#title' => $this->t('Gender'),
      '#required' => TRUE,
      '#options' => [
        '1' => $this->t('Male'),
        '2' => $this->t('Female'),
      ],
    ];

    $form['payment_details']['date'] = [
      '#type' => 'fieldset',
      '#title' => t('Date of birth'),
      '#states' => [
        'visible' => [
          ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'person'],
        ],
      ],
    ];

    $form['payment_details']['date']['byjuno_date_of_birth'] = [
      '#type' => 'datelist',
      '#date_part_order' => ['day', 'month' , 'year'],
      '#date_year_range' => '-99:-18',
      '#pre_render' => [
        [get_class($this), 'preRenderDateOfBirth']
      ],
      '#states' => [
        'required' => [
          ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'person'],
        ],
      ],
    ];

    if (count($payment_gateway_configuration['person_payment_methods']) == 1) {
      $form['payment_details']['byjuno_person_payment_type'] = [
        '#type' => 'hidden',
        '#value' => $payment_gateway_configuration['person_payment_methods'][0],
      ];
    } elseif (count($payment_gateway_configuration['person_payment_methods']) > 1) {
      $form['payment_details']['byjuno_person_payment_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Payment Option'),
        '#states' => [
          'visible' => [
            ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'person'],
          ],
          'required' => [
            ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'person'],
          ],
        ],
        '#options' => array_intersect_key($payment_method_options, array_flip($payment_gateway_configuration['person_payment_methods'])),
      ];
    }

    $form['payment_details']['byjuno_company_uid'] = [
      '#type' => 'textfield',
      '#title' => t('Company identification number'),
      '#states' => [
        'visible' => [
          ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'company'],
        ],
      ],
    ];

    if (count($payment_gateway_configuration['company_payment_methods']) == 1) {
      $form['payment_details']['byjuno_company_payment_type'] = [
        '#type' => 'hidden',
        '#value' => $payment_gateway_configuration['company_payment_methods'][0],
      ];
    } elseif (count($payment_gateway_configuration['company_payment_methods']) > 1) {
      $form['payment_details']['byjuno_company_payment_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Payment Option'),
        '#states' => [
          'visible' => [
            ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'company'],
          ],
          'required' => [
            ':input[name="payment_information[add_payment_method][payment_details][byjuno_customer_type]"]' => ['value' => 'company'],
          ],
        ],
        '#options' => array_intersect_key($payment_method_options, array_flip($payment_gateway_configuration['company_payment_methods'])),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    if ($values['payment_details']['byjuno_customer_type'] == 'person' && !isset($values['payment_details']['date']['byjuno_date_of_birth'])) {
      $form_state->setError($form['payment_details']['date']['byjuno_date_of_birth'], $this->t('Date of Birth is required for Private Individuals.'));
    }

    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
    if ($values['payment_details']['byjuno_customer_type'] == 'company') {
      $billing_profile = NULL;
      if ($form_state->has('billing_profile')) {
        $billing_profile = $form_state->get('billing_profile');
      }
      elseif (isset($form['billing_information']['#inline_form'])) {
        $inline_form = $form['billing_information']['#inline_form'];
        /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
        $billing_profile = $inline_form->getEntity();
      }

      if ($billing_profile && $billing_profile->address->organization == '') {
        $form_state->setError($form['payment_details'], $this->t('Organization name is required for Companies.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo This order is loaded from the routematch - if shipping is on the same page the details will not be included or updated when creating the payment method.
    if($this->order) {
      if ($form_state->has('billing_profile')) {
        $this->order->setBillingProfile($form_state->get('billing_profile'));
      }
      $order_parents = array_merge($form['#parents'], ['payment_details'], ['order']);
      $form_state->setValue($order_parents, $this->order);
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * @param $element
   *
   * @return array
   */
  public static function preRenderDateOfBirth($element) {
    $title = reset( $element['year']['#options'] );
    unset( $element['year']['#options'][ key($element['year']['#options'])]);
    arsort($element['year']['#options']);
    $element['year']['#options'] = [$title]+$element['year']['#options'];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderDateOfBirth'];
  }
}
