<?php

namespace Drupal\commerce_byjuno\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\AuthenticationException;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "byjuno",
 *   label = "CembraPay AG Invoice",
 *   display_label = "CembraPay AG Invoice",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_byjuno\PluginForm\ByjunoPaymentMethodAddForm",
 *   },
 *   payment_method_types = {"byjuno_invoice"},
 *   requires_billing_information = TRUE,
 * )
 */
class Byjuno extends OnsitePaymentGatewayBase implements ByjunoInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $permitted_response_codes = ['2'];
    return [
      'client_id' => '',
        'user_id' => '',
        'user_pw' => '',
        'person_payment_methods' => '',
        'company_payment_methods' => '',
        'paper_invoice' => FALSE,
        'permitted_response_codes' => $permitted_response_codes,
        'information_message' => [
          'format' => 'full_html',
          'value' => '',
        ],
        'declined_message' => [
          'format' => 'full_html',
          'value' => '',
        ],
        'append_response_message' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Example credential. Also needs matching schema in
    // config/schema/$your_module.schema.yml.
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $this->configuration['client_id'],
      '#required' => TRUE,
    ];

    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#default_value' => $this->configuration['user_id'],
      '#required' => TRUE,
    ];

    $form['user_pw'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Password'),
      '#description' =>  $this->t('This password will be exported in configuration as plain text, we recommend using settings.php to set it.'),
      '#default_value' => $this->configuration['user_pw'],
      '#required' => TRUE,
    ];

    $form['payment_methods'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Payment Methods'),
    );

    $form['payment_methods']['person_payment_methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment Methods available to Private Customers'),
      '#description' => $this->t('Select the payment method options available to the customer; if only one is selected the customer will not be asked to choose.'),
      '#default_value' => $this->configuration['person_payment_methods'],
      '#options' => $this->getPaymentMethodOptions(),
    ];

    $form['payment_methods']['company_payment_methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment Methods available to Business Customers'),
      '#description' => $this->t('Select the payment method options available to the customer; if only one is selected the customer will not be asked to choose.'),
      '#default_value' => $this->configuration['company_payment_methods'],
      '#options' => $this->getPaymentMethodOptions(),
    ];

    $form['invoicing'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Invoicing'),
    );

    $form['invoicing']['paper_invoice'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send Paper Invoice'),
      '#description' => $this->t('If checked then the invoice will be sent by post (in this case postal fees will apply to customer or merchant as per Agreement between CembraPay AG and Merchant). When unchecked the invoice will be sent by email (free of charge).'),
      '#default_value' => $this->configuration['paper_invoice'],
    ];

    $form['permitted_response_codes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Permitted Response Codes'),
      '#description' => $this->t('If code 2 is returned, CembraPay AG will take over the Payment guarantee to 100%. With all the other codes it is also possible offer the payment transaction via CembraPay AG, but the default risk lays with the merchant in these cases (purchase with recourse).  Depending on product segment, customer margin, target group or other aspects it is sinful to, together with CembraPay AG, determine for which additional codes, except for Code 2, payment via invoice should be offered. For these additional codes the default risk lies with the merchant. The jointly decided codes would then be returned with the response code 28.'),
      '#default_value' => $this->configuration['permitted_response_codes'],
      '#options' => $this->getResponseCodes(),
    ];

    $form['information_message'] = [
      '#type' => 'text_format',
      '#title' => 'Information Message',
      '#format' => $this->configuration['information_message']['format'],
      '#default_value' => $this->configuration['information_message']['value'],
    ];

    $form['declined_message'] = [
      '#type' => 'text_format',
      '#title' => 'Declined Message',
      '#format' => $this->configuration['declined_message']['format'],
      '#default_value' => $this->configuration['declined_message']['value'],
    ];

    $form['append_response_message'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Append response message to Declined Message'),
      '#description' => $this->t('If checked the default response text, or the text set below, will be appended to the Declined Message.'),
      '#default_value' => $this->configuration['append_response_message'],
    ];

    $form['response_messages'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Response Messages'),
    );

    foreach ($this->getResponseCodes() as $response_code => $response_message) {
      $form['response_messages'][$response_code] = [
        '#type' => 'textfield',
        '#title' => $response_message,
        '#default_value' => $this->configuration['response_messages'][$response_code] ?? '',
      ];
    }

    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $enabled_payment_options = count(array_keys(array_filter($values['payment_methods']['person_payment_methods'])))
      + count(array_keys(array_filter($values['payment_methods']['company_payment_methods'])));

    if ($enabled_payment_options < 1) {
      $form_state->setError($form['payment_methods'], $this->t('At least one payment method option must be enabled'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['client_id'] = $values['client_id'];
      $this->configuration['user_id'] = $values['user_id'];
      $this->configuration['user_pw'] = $values['user_pw'];
      $this->configuration['paper_invoice'] = $values['invoicing']['paper_invoice'];
      $this->configuration['person_payment_methods'] = array_keys(array_filter($values['payment_methods']['person_payment_methods']));
      $this->configuration['company_payment_methods'] = array_keys(array_filter($values['payment_methods']['company_payment_methods']));
      $this->configuration['permitted_response_codes'] = array_keys(array_filter($values['permitted_response_codes']));
      $this->configuration['information_message'] = $values['information_message'];
      $this->configuration['declined_message'] = $values['declined_message'];
      $this->configuration['append_response_message'] = $values['append_response_message'];
      foreach ($values['response_messages'] as $response_code => $response_message) {
        $this->configuration['response_messages'][$response_code] = $response_message;
      }
    }
  }

  /**
   * Return a list of the Byjuno Payment Methods
   * @return array[]
   */
  private function getPaymentMethods() {
    return [
      '3' => [
        'PAYMENTMETHOD' => 'INVOICE',
        'REPAYMENTTYPE' => '3',
        '#title' => $this->t('Standard Invoice'),
      ],
      '4' => [
        'PAYMENTMETHOD' => 'INVOICE',
        'REPAYMENTTYPE' => '4',
        '#title' => $this->t('CembraPay AG Invoice'),
      ],
      '10' => [
        'PAYMENTMETHOD' => 'INSTALLMENT',
        'REPAYMENTTYPE' => '10',
        '#title' => $this->t('3 monthly installments'),
      ],
      '8' => [
        'PAYMENTMETHOD' => 'INSTALLMENT',
        'REPAYMENTTYPE' => '8',
        '#title' => $this->t('12 monthly installments'),
      ],
      '9' => [
        'PAYMENTMETHOD' => 'INSTALLMENT',
        'REPAYMENTTYPE' => '9',
        '#title' => $this->t('24 monthly installments'),
      ],
      '11' => [
        'PAYMENTMETHOD' => 'INSTALLMENT',
        'REPAYMENTTYPE' => '11',
        '#title' => $this->t('36 monthly installments'),
      ],
      '1' => [
        'PAYMENTMETHOD' => 'INSTALLMENT',
        'REPAYMENTTYPE' => '1',
        '#title' => $this->t('4 quarterly installments'),
      ]
    ];
  }

  /**
   * Return a list of Byjuno payment methods formatted for an options select.
   * @return array
   */
  public function getPaymentMethodOptions() {
    $options = [];
    foreach ($this->getPaymentMethods() as $method) {
      $options[$method['REPAYMENTTYPE']] = $method['#title'];
    }
    return $options;
  }

  private function getResponseCodes() {
    return [
      '1' => $this->t('There are serious negative indicators'),
      '2' => $this->t('All payment methods'),
      '3' => $this->t('Manual postprocessing'),
      '4' => $this->t('The address is incorrect'),
      '5' => $this->t('Enquiry exceeds the credit limit (The credit limit is specified in the cooperation agreement)'),
      '6' => $this->t('Customer specifications are not met (currently not in use)'),
      '7' => $this->t('Enquiry exceeds the net credit limit (enquiry amount plus open items exceeds credit limit)'),
      '8' => $this->t('Person queried is not of creditworthy age'),
      '9' => $this->t('Delivery address does not match the invoice address'),
      '10' => $this->t('Household / company cannot be identified on the address'),
      '11' => $this->t('Country is not supported'),
      '12' => $this->t('Party queried is not a natural person'),
      '13' => $this->t('System is in maintenance mode'),
      '14' => $this->t('Address with high fraud risk'),
      '15' => $this->t('Allowance is to low'),
      '16' => $this->t('Application data is incomplete'),
      '17' => $this->t('Send contract documents for external credit check'),
      '18' => $this->t('External credit check in progress'),
      '19' => $this->t('Customer is on client blacklist'),
      '24' => $this->t('Ordering person not legitimated to order for company'),
      '27' => $this->t('Accepted with rights of withdrawal within 24 hours'),
      '28' => $this->t('Credit decision OK / Risk transferred to client due to exceeded credit limit'),
      '29' => $this->t('Maximum client limit exceeded'),
      '30' => $this->t('Unpaid overdue invoices exist'),
      '31' => $this->t('Request was made on an individual rather than a company'),
      '54' => $this->t('Consumer Limit exceeded'),
    ];
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment_details['order'];
    // Check if we have a byjuno payment already.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $payment_storage->loadMultipleByOrder($order);
    $process = TRUE;
    foreach ($payments as $payment) {
      if ($payment->getPaymentGateway()->id() === 'byjuno') {
        $process = FALSE;
      }
    }
    if ($process) {
      $payment_method->setReusable(FALSE);
      $payment_method->set('byjuno_customer_type', $payment_details['byjuno_customer_type']);
      $payment_method->set('byjuno_gender', $payment_details['byjuno_gender']);

      if ($payment_details['byjuno_customer_type'] == 'person' && isset($payment_details['byjuno_person_payment_type'])) {
        $payment_type = $payment_details['byjuno_person_payment_type'];
        /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
        $date = $payment_details['date']['byjuno_date_of_birth'];
        $payment_method->set('byjuno_date_of_birth', $date->format(\Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface::DATE_STORAGE_FORMAT));
      } elseif ($payment_details['byjuno_customer_type'] == 'company' && isset($payment_details['byjuno_company_payment_type'])) {
        $payment_type = $payment_details['byjuno_company_payment_type'];
        $payment_method->set('byjuno_uid', $payment_details['byjuno_company_uid']);
      } else {
        throw new PaymentGatewayException($this->t('Payment Method Type not selected'));
      }

      if (in_array($payment_type, ['3', '4'])) {
        $payment_method->set('byjuno_payment_method', 'INVOICE');
      } else {
        $payment_method->set('byjuno_payment_method', 'INSTALLMENT');
      }

      $payment_method->set('byjuno_payment_type', $payment_type);
      $payment_method = $this->reservePayment($order, $payment_method);
      $payment_method->save();
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // TODO: Cancel the payment reservation
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function reservePayment(OrderInterface $order, PaymentMethodInterface $payment_method) {
    // S1 Request for Reservation.
    $client = $this->createByjunoClient();

    $reservation_response = $client->reservationRequest($order, $payment_method);
    $gateway_configuration = $payment_method->getPaymentGateway()->getPluginConfiguration();

    if ($gateway_configuration['mode'] == 'test') {
      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . print_r($reservation_response, TRUE) . '</code></pre>');
    }

    $customer = $reservation_response->getCustomer()[0];
    if (in_array($customer->getRequestStatus(), $gateway_configuration['permitted_response_codes'])) {
      $payment_method->setRemoteId($customer->getTransactionNumber());
      return $payment_method;
    }
    else {
      $message = $gateway_configuration['declined_message'];
      if ($gateway_configuration['append_response_message']) {
        $message['value'] .= $gateway_configuration['response_messages'][$customer->getRequestStatus()] ? $gateway_configuration['response_messages'][$customer->getRequestStatus()] : $this->getResponseCodes()[$customer->getRequestStatus()];
      }
      \Drupal::messenger()->addWarning(check_markup($message['value'], $message['format']));
      throw new DeclineException($this->getResponseCodes()[$customer->getRequestStatus()]);
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // S3 Reservation Confirmation.
    $client = $this->createByjunoClient();

    $reservation_confirmation = $client->reservationConfirmation($payment, $capture);

    if ($payment->getPaymentGateway()->getPluginConfiguration()['mode'] == 'test') {
      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . print_r($reservation_confirmation, TRUE) . '</code></pre>');
    }

    $payment->setRemoteId($reservation_confirmation->getResponseId());
    $payment->setRemoteState($reservation_confirmation->getCustomer()[0]->getRequestStatus());
    $payment->setAuthorizedTime($reservation_confirmation->getCustomer()[0]->getLastStatusChange()->getTimestamp());

    $payment->setState('authorization');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $amount = $amount ?: $payment->getAmount();

    $client = $this->createByjunoClient();

    $settlement_confirmation = $client->settlement($payment, $amount);

    if ($payment->getPaymentGateway()->getPluginConfiguration()['mode'] == 'test') {
      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . print_r($settlement_confirmation, TRUE) . '</code></pre>');
    }

    if ($settlement_confirmation->getTransaction()->getProcessingInfo()->getCode() == 'TRX-00000') {
      if ($amount->equals($payment->getBalance())) {
        $payment->setState('completed');
      }
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $client = $this->createByjunoClient();

    $settlement_confirmation = $client->cancellation($payment);

    if ($payment->getPaymentGateway()->getPluginConfiguration()['mode'] == 'test') {
      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . print_r($settlement_confirmation, TRUE) . '</code></pre>');
    }

    if ($settlement_confirmation->getTransaction()->getProcessingInfo()->getCode() == 'TRX-00000') {
      $payment->setState('authorization_voided');
      $payment->save();
    }
  }

  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $client = $this->createByjunoClient();

    $settlement_confirmation = $client->refund($payment, $amount);

    if ($payment->getPaymentGateway()->getPluginConfiguration()['mode'] == 'test') {
      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . print_r($settlement_confirmation, TRUE) . '</code></pre>');
    }

    if ($settlement_confirmation->getTransaction()->getProcessingInfo()->getCode() == 'TRX-00000') {
      $old_refunded_amount = $payment->getRefundedAmount();
      $new_refunded_amount = $old_refunded_amount->add($amount);
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      }
      else {
        $payment->setState('refunded');
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createByjunoClient() {
    if (!isset($this->configuration['mode']) || !isset($this->configuration['user_id']) || !isset($this->configuration['user_pw']) || !isset($this->configuration['client_id'])) {
      throw new AuthenticationException('Unable to create Byjuno client because the payment gateway configuration does not specify a mode (environment) and access details.');
    }

    return \Drupal::service('commerce_byjuno.client');
  }

}
