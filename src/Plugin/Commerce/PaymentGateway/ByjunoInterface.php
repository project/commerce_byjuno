<?php


namespace Drupal\commerce_byjuno\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

interface ByjunoInterface extends SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Create a new Byjuno client based on this plugin's configuration.
   *
   * @return \Drupal\commerce_byjuno\Client\ByjunoClient
   *
   * @throws \Exception
   */
  public function createByjunoClient();

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *
   * @return mixed
   */
  public function reservePayment(OrderInterface $order, PaymentMethodInterface $payment_method);
}
