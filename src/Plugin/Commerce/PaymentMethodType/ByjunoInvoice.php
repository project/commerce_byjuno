<?php

namespace Drupal\commerce_byjuno\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the Byjuno payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "byjuno_invoice",
 *   label = @Translation("CembraPay AG Invoice"),
 * )
 */
class ByjunoInvoice extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $payment_method->getPaymentGateway()->getPluginConfiguration()['display_label'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['byjuno_customer_type'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Customer Type'))
      ->setDescription(t('The CembraPay AG Customer Type, Person or Company.'))
      ->setRequired(TRUE);

    $fields['byjuno_date_of_birth'] = BundleFieldDefinition::create('datetime')
      ->setLabel(t('Customer Type'))
      ->setDescription(t('The Date of Birth for Private Customers.'))
      ->setRequired(FALSE);

    $fields['byjuno_gender'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Gender'))
      ->setDescription(t('The customer gender.'))
      ->setRequired(TRUE);

    $fields['byjuno_uid'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Company UID'))
      ->setDescription(t('The Swiss Company UID.'))
      ->setRequired(FALSE);

    $fields['byjuno_payment_method'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Payment Method'))
      ->setDescription(t('The CembraPay AG payment method.'))
      ->setRequired(TRUE);

    $fields['byjuno_payment_type'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Payment Type'))
      ->setDescription(t('The CembraPay AG payment type id.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
