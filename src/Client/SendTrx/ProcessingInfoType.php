<?php

namespace Drupal\commerce_byjuno\Client\SendTrx;

/**
 * Class representing ProcessingInfoType
 *
 *
 * XSD Type: ProcessingInfoType
 */
class ProcessingInfoType
{

    /**
     * Coded information about processing
     *
     * @var string $code
     */
    private $code = null;

    /**
     * Classification of processing code
     *
     * @var string $classification
     */
    private $classification = null;

    /**
     * Common speech information about processing
     *
     * @var string $description
     */
    private $description = null;

    /**
     * Gets as code
     *
     * Coded information about processing
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets a new code
     *
     * Coded information about processing
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Gets as classification
     *
     * Classification of processing code
     *
     * @return string
     */
    public function getClassification()
    {
        return $this->classification;
    }

    /**
     * Sets a new classification
     *
     * Classification of processing code
     *
     * @param string $classification
     * @return self
     */
    public function setClassification($classification)
    {
        $this->classification = $classification;
        return $this;
    }

    /**
     * Gets as description
     *
     * Common speech information about processing
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets a new description
     *
     * Common speech information about processing
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


}

