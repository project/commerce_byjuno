<?php

namespace Drupal\commerce_byjuno\Client\SendTrx\Response;

/**
 * Class representing ResponseAType
 */
class ResponseAType
{

    /**
     * Intrum's client identifier (derived from Intrums request)
     *
     * @var int $clientId
     */
    private $clientId = null;

    /**
     * Intrums unique request identification (derived from Intrums request)
     *
     * @var string $requestId
     */
    private $requestId = null;

    /**
     * Version number denoting the schemaversion used for request/response pair.
     *
     * @var float $version
     */
    private $version = null;

    /**
     * Clients unique response identification
     *
     * @var string $responseId
     */
    private $responseId = null;

    /**
     * Identifier for the clients customer
     *
     * @var \Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType\TransactionAType $transaction
     */
    private $transaction = null;

    /**
     * Gets as clientId
     *
     * Intrum's client identifier (derived from Intrums request)
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets a new clientId
     *
     * Intrum's client identifier (derived from Intrums request)
     *
     * @param int $clientId
     * @return self
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Gets as requestId
     *
     * Intrums unique request identification (derived from Intrums request)
     *
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Sets a new requestId
     *
     * Intrums unique request identification (derived from Intrums request)
     *
     * @param string $requestId
     * @return self
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * Gets as version
     *
     * Version number denoting the schemaversion used for request/response pair.
     *
     * @return float
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets a new version
     *
     * Version number denoting the schemaversion used for request/response pair.
     *
     * @param float $version
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Gets as responseId
     *
     * Clients unique response identification
     *
     * @return string
     */
    public function getResponseId()
    {
        return $this->responseId;
    }

    /**
     * Sets a new responseId
     *
     * Clients unique response identification
     *
     * @param string $responseId
     * @return self
     */
    public function setResponseId($responseId)
    {
        $this->responseId = $responseId;
        return $this;
    }

    /**
     * Gets as transaction
     *
     * Identifier for the clients customer
     *
     * @return \Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType\TransactionAType
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Sets a new transaction
     *
     * Identifier for the clients customer
     *
     * @param \Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType\TransactionAType $transaction
     * @return self
     */
    public function setTransaction(\Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType\TransactionAType $transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }


}

