<?php

namespace Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType;

/**
 * Class representing TransactionAType
 */
class TransactionAType
{

    /**
     * @var string $orderId
     */
    private $orderId = null;

    /**
     * @var string $clientRef
     */
    private $clientRef = null;

    /**
     * Information about request processing
     *
     * @var \Drupal\commerce_byjuno\Client\SendTrx\ProcessingInfoType $processingInfo
     */
    private $processingInfo = null;

    /**
     * Gets as orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Sets a new orderId
     *
     * @param string $orderId
     * @return self
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * Gets as clientRef
     *
     * @return string
     */
    public function getClientRef()
    {
        return $this->clientRef;
    }

    /**
     * Sets a new clientRef
     *
     * @param string $clientRef
     * @return self
     */
    public function setClientRef($clientRef)
    {
        $this->clientRef = $clientRef;
        return $this;
    }

    /**
     * Gets as processingInfo
     *
     * Information about request processing
     *
     * @return \Drupal\commerce_byjuno\Client\SendTrx\ProcessingInfoType
     */
    public function getProcessingInfo()
    {
        return $this->processingInfo;
    }

    /**
     * Sets a new processingInfo
     *
     * Information about request processing
     *
     * @param \Drupal\commerce_byjuno\Client\SendTrx\ProcessingInfoType $processingInfo
     * @return self
     */
    public function setProcessingInfo(\Drupal\commerce_byjuno\Client\SendTrx\ProcessingInfoType $processingInfo)
    {
        $this->processingInfo = $processingInfo;
        return $this;
    }


}

