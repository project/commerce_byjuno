<?php

namespace Drupal\commerce_byjuno\Client\SendTrx\Request;

/**
 * Class representing RequestAType
 */
class RequestAType
{

    /**
     * Ihre Kundennummer bei Intrum Justitia
     *
     * @var int $clientId
     */
    private $clientId = null;

    /**
     * Versionsnummer der Schnittstelle
     *
     * @var float $version
     */
    private $version = null;

    /**
     * @var string $siteId
     */
    private $siteId = null;

    /**
     * @var string $siteSubId
     */
    private $siteSubId = null;

    /**
     * Email Adresse des Kontaktes bei Ihnen. Dient zur Kontaktaufnahme bei technischen Problemen
     *
     * @var string $email
     */
    private $email = null;

    /**
     * Unique identification of Request.
     *
     * @var string $requestId
     */
    private $requestId = null;

    /**
     * @var string $userID
     */
    private $userID = null;

    /**
     * @var string $password
     */
    private $password = null;

    /**
     * Customer to be checked
     *
     * @var \Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType\TransactionAType[] $transaction
     */
    private $transaction = [
        
    ];

    /**
     * Gets as clientId
     *
     * Ihre Kundennummer bei Intrum Justitia
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets a new clientId
     *
     * Ihre Kundennummer bei Intrum Justitia
     *
     * @param int $clientId
     * @return self
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Gets as version
     *
     * Versionsnummer der Schnittstelle
     *
     * @return float
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets a new version
     *
     * Versionsnummer der Schnittstelle
     *
     * @param float $version
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Gets as siteId
     *
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * Sets a new siteId
     *
     * @param string $siteId
     * @return self
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * Gets as siteSubId
     *
     * @return string
     */
    public function getSiteSubId()
    {
        return $this->siteSubId;
    }

    /**
     * Sets a new siteSubId
     *
     * @param string $siteSubId
     * @return self
     */
    public function setSiteSubId($siteSubId)
    {
        $this->siteSubId = $siteSubId;
        return $this;
    }

    /**
     * Gets as email
     *
     * Email Adresse des Kontaktes bei Ihnen. Dient zur Kontaktaufnahme bei technischen Problemen
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets a new email
     *
     * Email Adresse des Kontaktes bei Ihnen. Dient zur Kontaktaufnahme bei technischen Problemen
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Gets as requestId
     *
     * Unique identification of Request.
     *
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Sets a new requestId
     *
     * Unique identification of Request.
     *
     * @param string $requestId
     * @return self
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * Gets as userID
     *
     * @return string
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * Sets a new userID
     *
     * @param string $userID
     * @return self
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * Gets as password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets a new password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Adds as transaction
     *
     * Customer to be checked
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType\TransactionAType $transaction
     */
    public function addToTransaction(\Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType\TransactionAType $transaction)
    {
        $this->transaction[] = $transaction;
        return $this;
    }

    /**
     * isset transaction
     *
     * Customer to be checked
     *
     * @param int|string $index
     * @return bool
     */
    public function issetTransaction($index)
    {
        return isset($this->transaction[$index]);
    }

    /**
     * unset transaction
     *
     * Customer to be checked
     *
     * @param int|string $index
     * @return void
     */
    public function unsetTransaction($index)
    {
        unset($this->transaction[$index]);
    }

    /**
     * Gets as transaction
     *
     * Customer to be checked
     *
     * @return \Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType\TransactionAType[]
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Sets a new transaction
     *
     * Customer to be checked
     *
     * @param \Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType\TransactionAType[] $transaction
     * @return self
     */
    public function setTransaction(array $transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }


}

