<?php

namespace Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType;

/**
 * Class representing TransactionAType
 */
class TransactionAType
{

    /**
     * @var string $orderId
     */
    private $orderId = null;

    /**
     * @var string $clientRef
     */
    private $clientRef = null;

    /**
     * @var string $transactionType
     */
    private $transactionType = null;

    /**
     * @var \DateTime $transactionDate
     */
    private $transactionDate = null;

    /**
     * @var float $transactionAmount
     */
    private $transactionAmount = null;

    /**
     * @var string $transactionCurrency
     */
    private $transactionCurrency = null;

    /**
     * @var string $additional1
     */
    private $additional1 = null;

    /**
     * @var string $additional2
     */
    private $additional2 = null;

    /**
     * @var string $additional3
     */
    private $additional3 = null;

    /**
     * @var float $openBalance
     */
    private $openBalance = null;

    /**
     * @var string $reminderLevel
     */
    private $reminderLevel = null;

    /**
     * @var int $paymentTermType
     */
    private $paymentTermType = null;

    /**
     * @var int $daysToDue
     */
    private $daysToDue = null;

    /**
     * @var int $daysToExecution
     */
    private $daysToExecution = null;

    /**
     * @var int $numberOfInstallments
     */
    private $numberOfInstallments = null;

    /**
     * @var int $daysBetweenInstallments
     */
    private $daysBetweenInstallments = null;

    /**
     * @var string $billingAccountReference
     */
    private $billingAccountReference = null;

    /**
     * @var string $partialDelivery
     */
    private $partialDelivery = null;

    /**
     * Gets as orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Sets a new orderId
     *
     * @param string $orderId
     * @return self
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * Gets as clientRef
     *
     * @return string
     */
    public function getClientRef()
    {
        return $this->clientRef;
    }

    /**
     * Sets a new clientRef
     *
     * @param string $clientRef
     * @return self
     */
    public function setClientRef($clientRef)
    {
        $this->clientRef = $clientRef;
        return $this;
    }

    /**
     * Gets as transactionType
     *
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * Sets a new transactionType
     *
     * @param string $transactionType
     * @return self
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;
        return $this;
    }

    /**
     * Gets as transactionDate
     *
     * @return \DateTime
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Sets a new transactionDate
     *
     * @param \DateTime $transactionDate
     * @return self
     */
    public function setTransactionDate(\DateTime $transactionDate)
    {
        $this->transactionDate = $transactionDate;
        return $this;
    }

    /**
     * Gets as transactionAmount
     *
     * @return float
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * Sets a new transactionAmount
     *
     * @param float $transactionAmount
     * @return self
     */
    public function setTransactionAmount($transactionAmount)
    {
        $this->transactionAmount = $transactionAmount;
        return $this;
    }

    /**
     * Gets as transactionCurrency
     *
     * @return string
     */
    public function getTransactionCurrency()
    {
        return $this->transactionCurrency;
    }

    /**
     * Sets a new transactionCurrency
     *
     * @param string $transactionCurrency
     * @return self
     */
    public function setTransactionCurrency($transactionCurrency)
    {
        $this->transactionCurrency = $transactionCurrency;
        return $this;
    }

    /**
     * Gets as additional1
     *
     * @return string
     */
    public function getAdditional1()
    {
        return $this->additional1;
    }

    /**
     * Sets a new additional1
     *
     * @param string $additional1
     * @return self
     */
    public function setAdditional1($additional1)
    {
        $this->additional1 = $additional1;
        return $this;
    }

    /**
     * Gets as additional2
     *
     * @return string
     */
    public function getAdditional2()
    {
        return $this->additional2;
    }

    /**
     * Sets a new additional2
     *
     * @param string $additional2
     * @return self
     */
    public function setAdditional2($additional2)
    {
        $this->additional2 = $additional2;
        return $this;
    }

    /**
     * Gets as additional3
     *
     * @return string
     */
    public function getAdditional3()
    {
        return $this->additional3;
    }

    /**
     * Sets a new additional3
     *
     * @param string $additional3
     * @return self
     */
    public function setAdditional3($additional3)
    {
        $this->additional3 = $additional3;
        return $this;
    }

    /**
     * Gets as openBalance
     *
     * @return float
     */
    public function getOpenBalance()
    {
        return $this->openBalance;
    }

    /**
     * Sets a new openBalance
     *
     * @param float $openBalance
     * @return self
     */
    public function setOpenBalance($openBalance)
    {
        $this->openBalance = $openBalance;
        return $this;
    }

    /**
     * Gets as reminderLevel
     *
     * @return string
     */
    public function getReminderLevel()
    {
        return $this->reminderLevel;
    }

    /**
     * Sets a new reminderLevel
     *
     * @param string $reminderLevel
     * @return self
     */
    public function setReminderLevel($reminderLevel)
    {
        $this->reminderLevel = $reminderLevel;
        return $this;
    }

    /**
     * Gets as paymentTermType
     *
     * @return int
     */
    public function getPaymentTermType()
    {
        return $this->paymentTermType;
    }

    /**
     * Sets a new paymentTermType
     *
     * @param int $paymentTermType
     * @return self
     */
    public function setPaymentTermType($paymentTermType)
    {
        $this->paymentTermType = $paymentTermType;
        return $this;
    }

    /**
     * Gets as daysToDue
     *
     * @return int
     */
    public function getDaysToDue()
    {
        return $this->daysToDue;
    }

    /**
     * Sets a new daysToDue
     *
     * @param int $daysToDue
     * @return self
     */
    public function setDaysToDue($daysToDue)
    {
        $this->daysToDue = $daysToDue;
        return $this;
    }

    /**
     * Gets as daysToExecution
     *
     * @return int
     */
    public function getDaysToExecution()
    {
        return $this->daysToExecution;
    }

    /**
     * Sets a new daysToExecution
     *
     * @param int $daysToExecution
     * @return self
     */
    public function setDaysToExecution($daysToExecution)
    {
        $this->daysToExecution = $daysToExecution;
        return $this;
    }

    /**
     * Gets as numberOfInstallments
     *
     * @return int
     */
    public function getNumberOfInstallments()
    {
        return $this->numberOfInstallments;
    }

    /**
     * Sets a new numberOfInstallments
     *
     * @param int $numberOfInstallments
     * @return self
     */
    public function setNumberOfInstallments($numberOfInstallments)
    {
        $this->numberOfInstallments = $numberOfInstallments;
        return $this;
    }

    /**
     * Gets as daysBetweenInstallments
     *
     * @return int
     */
    public function getDaysBetweenInstallments()
    {
        return $this->daysBetweenInstallments;
    }

    /**
     * Sets a new daysBetweenInstallments
     *
     * @param int $daysBetweenInstallments
     * @return self
     */
    public function setDaysBetweenInstallments($daysBetweenInstallments)
    {
        $this->daysBetweenInstallments = $daysBetweenInstallments;
        return $this;
    }

    /**
     * Gets as billingAccountReference
     *
     * @return string
     */
    public function getBillingAccountReference()
    {
        return $this->billingAccountReference;
    }

    /**
     * Sets a new billingAccountReference
     *
     * @param string $billingAccountReference
     * @return self
     */
    public function setBillingAccountReference($billingAccountReference)
    {
        $this->billingAccountReference = $billingAccountReference;
        return $this;
    }

    /**
     * Gets as partialDelivery
     *
     * @return string
     */
    public function getPartialDelivery()
    {
        return $this->partialDelivery;
    }

    /**
     * Sets a new partialDelivery
     *
     * @param string $partialDelivery
     * @return self
     */
    public function setPartialDelivery($partialDelivery)
    {
        $this->partialDelivery = $partialDelivery;
        return $this;
    }


}

