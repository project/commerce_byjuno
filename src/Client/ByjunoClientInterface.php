<?php


namespace Drupal\commerce_byjuno\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

interface ByjunoClientInterface extends ContainerInjectionInterface {

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *
   * @return \Drupal\commerce_byjuno\Client\CreditDecision\Response
   */
  public function reservationRequest(OrderInterface $order, PaymentMethodInterface $payment_method);

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param bool $capture
   *
   * @return \Drupal\commerce_byjuno\Client\CreditDecision\Response
   */
  public function reservationConfirmation(PaymentInterface $payment, $capture = FALSE);

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @return \Drupal\commerce_byjuno\Client\SendTrx\Response
   */
  public function settlement(PaymentInterface $payment);

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @return \Drupal\commerce_byjuno\Client\SendTrx\Response
   */
  public function refund(PaymentInterface $payment);

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @return \Drupal\commerce_byjuno\Client\SendTrx\Response
   */
  public function cancellation(PaymentInterface $payment);

}
