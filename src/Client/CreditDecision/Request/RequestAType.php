<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision\Request;

/**
 * Class representing RequestAType
 */
class RequestAType
{

    /**
     * your client identification no. at Intrum Justitia
     *
     * @var int $clientId
     */
    private $clientId = null;

    /**
     * interface version
     *
     * @var float $version
     */
    private $version = null;

    /**
     * @var string $siteId
     */
    private $siteId = null;

    /**
     * @var string $siteSubId
     */
    private $siteSubId = null;

    /**
     * email address of your technical contact
     *
     * @var string $email
     */
    private $email = null;

    /**
     * unique identification of credit check request
     *
     * @var string $requestId
     */
    private $requestId = null;

    /**
     * @var string $userID
     */
    private $userID = null;

    /**
     * @var string $password
     */
    private $password = null;

    /**
     * customer to be checked
     *
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType\CustomerAType[] $customer
     */
    private $customer = [
        
    ];

    /**
     * Gets as clientId
     *
     * your client identification no. at Intrum Justitia
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets a new clientId
     *
     * your client identification no. at Intrum Justitia
     *
     * @param int $clientId
     * @return self
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Gets as version
     *
     * interface version
     *
     * @return float
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets a new version
     *
     * interface version
     *
     * @param float $version
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Gets as siteId
     *
     * @return string
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * Sets a new siteId
     *
     * @param string $siteId
     * @return self
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * Gets as siteSubId
     *
     * @return string
     */
    public function getSiteSubId()
    {
        return $this->siteSubId;
    }

    /**
     * Sets a new siteSubId
     *
     * @param string $siteSubId
     * @return self
     */
    public function setSiteSubId($siteSubId)
    {
        $this->siteSubId = $siteSubId;
        return $this;
    }

    /**
     * Gets as email
     *
     * email address of your technical contact
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets a new email
     *
     * email address of your technical contact
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Gets as requestId
     *
     * unique identification of credit check request
     *
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Sets a new requestId
     *
     * unique identification of credit check request
     *
     * @param string $requestId
     * @return self
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * Gets as userID
     *
     * @return string
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * Sets a new userID
     *
     * @param string $userID
     * @return self
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * Gets as password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets a new password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Adds as customer
     *
     * customer to be checked
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType\CustomerAType $customer
     */
    public function addToCustomer(\Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType\CustomerAType $customer)
    {
        $this->customer[] = $customer;
        return $this;
    }

    /**
     * isset customer
     *
     * customer to be checked
     *
     * @param int|string $index
     * @return bool
     */
    public function issetCustomer($index)
    {
        return isset($this->customer[$index]);
    }

    /**
     * unset customer
     *
     * customer to be checked
     *
     * @param int|string $index
     * @return void
     */
    public function unsetCustomer($index)
    {
        unset($this->customer[$index]);
    }

    /**
     * Gets as customer
     *
     * customer to be checked
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType\CustomerAType[]
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Sets a new customer
     *
     * customer to be checked
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType\CustomerAType[] $customer
     * @return self
     */
    public function setCustomer(array $customer)
    {
        $this->customer = $customer;
        return $this;
    }


}

