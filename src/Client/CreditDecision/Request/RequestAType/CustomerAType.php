<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType;

/**
 * Class representing CustomerAType
 */
class CustomerAType
{

    /**
     * unique identification of customer
     *
     * @var string $reference
     */
    private $reference = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\Person $person
     */
    private $person = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\Company $company
     */
    private $company = null;

    /**
     * Gets as reference
     *
     * unique identification of customer
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Sets a new reference
     *
     * unique identification of customer
     *
     * @param string $reference
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Gets as person
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets a new person
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Person $person
     * @return self
     */
    public function setPerson(\Drupal\commerce_byjuno\Client\CreditDecision\Person $person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * Gets as company
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets a new company
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Company $company
     * @return self
     */
    public function setCompany(\Drupal\commerce_byjuno\Client\CreditDecision\Company $company)
    {
        $this->company = $company;
        return $this;
    }


}

