<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing OrderingPersonType
 *
 *
 * XSD Type: OrderingPersonType
 */
class OrderingPersonType
{

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\PersonType $person
     */
    private $person = null;

    /**
     * @var int $function
     */
    private $function = null;

    /**
     * Gets as person
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\PersonType
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets a new person
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\PersonType $person
     * @return self
     */
    public function setPerson(\Drupal\commerce_byjuno\Client\CreditDecision\PersonType $person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * Gets as function
     *
     * @return int
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Sets a new function
     *
     * @param int $function
     * @return self
     */
    public function setFunction($function)
    {
        $this->function = $function;
        return $this;
    }


}

