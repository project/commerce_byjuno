<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing AddressType
 *
 *
 * XSD Type: AddressType
 */
class AddressType
{

    /**
     * @var string $firstLine
     */
    private $firstLine = null;

    /**
     * @var string $secondLine
     */
    private $secondLine = null;

    /**
     * @var string $houseNumber
     */
    private $houseNumber = null;

    /**
     * countrycode ISO2 or ISO3
     *
     * @var string $countryCode
     */
    private $countryCode = null;

    /**
     * @var string $postCode
     */
    private $postCode = null;

    /**
     * @var string $town
     */
    private $town = null;

    /**
     * Gets as firstLine
     *
     * @return string
     */
    public function getFirstLine()
    {
        return $this->firstLine;
    }

    /**
     * Sets a new firstLine
     *
     * @param string $firstLine
     * @return self
     */
    public function setFirstLine($firstLine)
    {
        $this->firstLine = $firstLine;
        return $this;
    }

    /**
     * Gets as secondLine
     *
     * @return string
     */
    public function getSecondLine()
    {
        return $this->secondLine;
    }

    /**
     * Sets a new secondLine
     *
     * @param string $secondLine
     * @return self
     */
    public function setSecondLine($secondLine)
    {
        $this->secondLine = $secondLine;
        return $this;
    }

    /**
     * Gets as houseNumber
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Sets a new houseNumber
     *
     * @param string $houseNumber
     * @return self
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * Gets as countryCode
     *
     * countrycode ISO2 or ISO3
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Sets a new countryCode
     *
     * countrycode ISO2 or ISO3
     *
     * @param string $countryCode
     * @return self
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * Gets as postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Sets a new postCode
     *
     * @param string $postCode
     * @return self
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * Gets as town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Sets a new town
     *
     * @param string $town
     * @return self
     */
    public function setTown($town)
    {
        $this->town = $town;
        return $this;
    }


}

