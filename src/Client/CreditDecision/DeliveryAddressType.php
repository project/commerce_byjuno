<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing DeliveryAddressType
 *
 *
 * XSD Type: DeliveryAddressType
 */
class DeliveryAddressType
{

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\Company $company
     */
    private $company = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\Person $person
     */
    private $person = null;

    /**
     * Gets as company
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets a new company
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Company $company
     * @return self
     */
    public function setCompany(\Drupal\commerce_byjuno\Client\CreditDecision\Company $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Gets as person
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets a new person
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Person $person
     * @return self
     */
    public function setPerson(\Drupal\commerce_byjuno\Client\CreditDecision\Person $person)
    {
        $this->person = $person;
        return $this;
    }


}

