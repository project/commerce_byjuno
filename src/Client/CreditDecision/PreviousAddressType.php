<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing PreviousAddressType
 *
 *
 * XSD Type: PreviousAddressType
 */
class PreviousAddressType
{

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\AddressType $address
     */
    private $address = null;

    /**
     * @var \DateTime $endDate
     */
    private $endDate = null;

    /**
     * Gets as address
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\AddressType
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets a new address
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\AddressType $address
     * @return self
     */
    public function setAddress(\Drupal\commerce_byjuno\Client\CreditDecision\AddressType $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Gets as endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Sets a new endDate
     *
     * @param \DateTime $endDate
     * @return self
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }


}

