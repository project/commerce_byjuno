<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision\PurchaseType;

/**
 * Class representing ProductCategoriesAType
 */
class ProductCategoriesAType
{

    /**
     * @var int[] $productCategory
     */
    private $productCategory = [
        
    ];

    /**
     * Adds as productCategory
     *
     * @return self
     * @param int $productCategory
     */
    public function addToProductCategory($productCategory)
    {
        $this->productCategory[] = $productCategory;
        return $this;
    }

    /**
     * isset productCategory
     *
     * @param int|string $index
     * @return bool
     */
    public function issetProductCategory($index)
    {
        return isset($this->productCategory[$index]);
    }

    /**
     * unset productCategory
     *
     * @param int|string $index
     * @return void
     */
    public function unsetProductCategory($index)
    {
        unset($this->productCategory[$index]);
    }

    /**
     * Gets as productCategory
     *
     * @return int[]
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * Sets a new productCategory
     *
     * @param int[] $productCategory
     * @return self
     */
    public function setProductCategory(array $productCategory)
    {
        $this->productCategory = $productCategory;
        return $this;
    }


}

