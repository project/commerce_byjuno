<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing PersonType
 *
 *
 * XSD Type: PersonType
 */
class PersonType
{

    /**
     * @var string $lastName
     */
    private $lastName = null;

    /**
     * @var string $firstName
     */
    private $firstName = null;

    /**
     * @var int $gender
     */
    private $gender = null;

    /**
     * @var \DateTime $dateOfBirth
     */
    private $dateOfBirth = null;

    /**
     * @var int $maritalStatus
     */
    private $maritalStatus = null;

    /**
     * @var string $language
     */
    private $language = null;

    /**
     * country Code ISO2 or ISO3
     *
     * @var string $nationality
     */
    private $nationality = null;

    /**
     * @var string $residencePermit
     */
    private $residencePermit = null;

    /**
     * @var \DateTime $residentSince
     */
    private $residentSince = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\AddressType $currentAddress
     */
    private $currentAddress = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType $previousAddress
     */
    private $previousAddress = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType $communicationNumbers
     */
    private $communicationNumbers = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[] $extraInfo
     */
    private $extraInfo = [
        
    ];

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType $deliveryAddress
     */
    private $deliveryAddress = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\PurchaseType $purchase
     */
    private $purchase = null;

    /**
     * Gets as lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets a new lastName
     *
     * @param string $lastName
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Gets as firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets a new firstName
     *
     * @param string $firstName
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Gets as gender
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets a new gender
     *
     * @param int $gender
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Gets as dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Sets a new dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return self
     */
    public function setDateOfBirth(\DateTime $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
        return $this;
    }

    /**
     * Gets as maritalStatus
     *
     * @return int
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Sets a new maritalStatus
     *
     * @param int $maritalStatus
     * @return self
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
        return $this;
    }

    /**
     * Gets as language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets a new language
     *
     * @param string $language
     * @return self
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * Gets as nationality
     *
     * country Code ISO2 or ISO3
     *
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Sets a new nationality
     *
     * country Code ISO2 or ISO3
     *
     * @param string $nationality
     * @return self
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
        return $this;
    }

    /**
     * Gets as residencePermit
     *
     * @return string
     */
    public function getResidencePermit()
    {
        return $this->residencePermit;
    }

    /**
     * Sets a new residencePermit
     *
     * @param string $residencePermit
     * @return self
     */
    public function setResidencePermit($residencePermit)
    {
        $this->residencePermit = $residencePermit;
        return $this;
    }

    /**
     * Gets as residentSince
     *
     * @return \DateTime
     */
    public function getResidentSince()
    {
        return $this->residentSince;
    }

    /**
     * Sets a new residentSince
     *
     * @param \DateTime $residentSince
     * @return self
     */
    public function setResidentSince(\DateTime $residentSince)
    {
        $this->residentSince = $residentSince;
        return $this;
    }

    /**
     * Gets as currentAddress
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\AddressType
     */
    public function getCurrentAddress()
    {
        return $this->currentAddress;
    }

    /**
     * Sets a new currentAddress
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\AddressType $currentAddress
     * @return self
     */
    public function setCurrentAddress(\Drupal\commerce_byjuno\Client\CreditDecision\AddressType $currentAddress)
    {
        $this->currentAddress = $currentAddress;
        return $this;
    }

    /**
     * Gets as previousAddress
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType
     */
    public function getPreviousAddress()
    {
        return $this->previousAddress;
    }

    /**
     * Sets a new previousAddress
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType $previousAddress
     * @return self
     */
    public function setPreviousAddress(\Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType $previousAddress)
    {
        $this->previousAddress = $previousAddress;
        return $this;
    }

    /**
     * Gets as communicationNumbers
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType
     */
    public function getCommunicationNumbers()
    {
        return $this->communicationNumbers;
    }

    /**
     * Sets a new communicationNumbers
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType $communicationNumbers
     * @return self
     */
    public function setCommunicationNumbers(\Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType $communicationNumbers)
    {
        $this->communicationNumbers = $communicationNumbers;
        return $this;
    }

    /**
     * Adds as extraInfo
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType $extraInfo
     */
    public function addToExtraInfo(\Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType $extraInfo)
    {
        $this->extraInfo[] = $extraInfo;
        return $this;
    }

    /**
     * isset extraInfo
     *
     * @param int|string $index
     * @return bool
     */
    public function issetExtraInfo($index)
    {
        return isset($this->extraInfo[$index]);
    }

    /**
     * unset extraInfo
     *
     * @param int|string $index
     * @return void
     */
    public function unsetExtraInfo($index)
    {
        unset($this->extraInfo[$index]);
    }

    /**
     * Gets as extraInfo
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[]
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Sets a new extraInfo
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[] $extraInfo
     * @return self
     */
    public function setExtraInfo(array $extraInfo)
    {
        $this->extraInfo = $extraInfo;
        return $this;
    }

    /**
     * Gets as deliveryAddress
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Sets a new deliveryAddress
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType $deliveryAddress
     * @return self
     */
    public function setDeliveryAddress(\Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType $deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
        return $this;
    }

    /**
     * Gets as purchase
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\PurchaseType
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * Sets a new purchase
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\PurchaseType $purchase
     * @return self
     */
    public function setPurchase(\Drupal\commerce_byjuno\Client\CreditDecision\PurchaseType $purchase)
    {
        $this->purchase = $purchase;
        return $this;
    }


}

