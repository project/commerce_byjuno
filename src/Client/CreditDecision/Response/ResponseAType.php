<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision\Response;

/**
 * Class representing ResponseAType
 */
class ResponseAType
{

    /**
     * Ihre Kundennummer bei Schimmelpfeng Auskunft
     *
     * @var int $clientId
     */
    private $clientId = null;

    /**
     * Versionsnummer der Schnittstelle
     *
     * @var float $version
     */
    private $version = null;

    /**
     * Identifikation der Anfrage. Muss vom Kunden mit gegeben werden.
     *
     * @var int $requestId
     */
    private $requestId = null;

    /**
     * @var int $responseId
     */
    private $responseId = null;

    /**
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType[] $processingInfo
     */
    private $processingInfo = [
        
    ];

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType\CustomerAType[] $customer
     */
    private $customer = [
        
    ];

    /**
     * Gets as clientId
     *
     * Ihre Kundennummer bei Schimmelpfeng Auskunft
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets a new clientId
     *
     * Ihre Kundennummer bei Schimmelpfeng Auskunft
     *
     * @param int $clientId
     * @return self
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Gets as version
     *
     * Versionsnummer der Schnittstelle
     *
     * @return float
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets a new version
     *
     * Versionsnummer der Schnittstelle
     *
     * @param float $version
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Gets as requestId
     *
     * Identifikation der Anfrage. Muss vom Kunden mit gegeben werden.
     *
     * @return int
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Sets a new requestId
     *
     * Identifikation der Anfrage. Muss vom Kunden mit gegeben werden.
     *
     * @param int $requestId
     * @return self
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * Gets as responseId
     *
     * @return int
     */
    public function getResponseId()
    {
        return $this->responseId;
    }

    /**
     * Sets a new responseId
     *
     * @param int $responseId
     * @return self
     */
    public function setResponseId($responseId)
    {
        $this->responseId = $responseId;
        return $this;
    }

    /**
     * Adds as processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType $processingInfo
     */
    public function addToProcessingInfo(\Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType $processingInfo)
    {
        $this->processingInfo[] = $processingInfo;
        return $this;
    }

    /**
     * isset processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @param int|string $index
     * @return bool
     */
    public function issetProcessingInfo($index)
    {
        return isset($this->processingInfo[$index]);
    }

    /**
     * unset processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @param int|string $index
     * @return void
     */
    public function unsetProcessingInfo($index)
    {
        unset($this->processingInfo[$index]);
    }

    /**
     * Gets as processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType[]
     */
    public function getProcessingInfo()
    {
        return $this->processingInfo;
    }

    /**
     * Sets a new processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType[] $processingInfo
     * @return self
     */
    public function setProcessingInfo(array $processingInfo)
    {
        $this->processingInfo = $processingInfo;
        return $this;
    }

    /**
     * Adds as customer
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType\CustomerAType $customer
     */
    public function addToCustomer(\Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType\CustomerAType $customer)
    {
        $this->customer[] = $customer;
        return $this;
    }

    /**
     * isset customer
     *
     * @param int|string $index
     * @return bool
     */
    public function issetCustomer($index)
    {
        return isset($this->customer[$index]);
    }

    /**
     * unset customer
     *
     * @param int|string $index
     * @return void
     */
    public function unsetCustomer($index)
    {
        unset($this->customer[$index]);
    }

    /**
     * Gets as customer
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType\CustomerAType[]
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Sets a new customer
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType\CustomerAType[] $customer
     * @return self
     */
    public function setCustomer(array $customer)
    {
        $this->customer = $customer;
        return $this;
    }


}

