<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType;

/**
 * Class representing CustomerAType
 */
class CustomerAType
{

    /**
     * Eindeutige Referenznummer des Geschäftsvorfalls bei Ihnen
     *
     * @var string $reference
     */
    private $reference = null;

    /**
     * @var int $iJReference
     */
    private $iJReference = null;

    /**
     * @var string $creditRating
     */
    private $creditRating = null;

    /**
     * @var string $creditRatingLevel
     */
    private $creditRatingLevel = null;

    /**
     * @var float $remainingLimit
     */
    private $remainingLimit = null;

    /**
     * @var string $creditDecision
     */
    private $creditDecision = null;

    /**
     * @var int $requestStatus
     */
    private $requestStatus = null;

    /**
     * @var \DateTime $lastStatusChange
     */
    private $lastStatusChange = null;

    /**
     * @var string $transactionNumber
     */
    private $transactionNumber = null;

    /**
     * @var int $decisionProcessNumber
     */
    private $decisionProcessNumber = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[] $extraInfo
     */
    private $extraInfo = [
        
    ];

    /**
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType[] $processingInfo
     */
    private $processingInfo = [
        
    ];

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\OrderHistoryType[] $orderHistory
     */
    private $orderHistory = [
        
    ];

    /**
     * Gets as reference
     *
     * Eindeutige Referenznummer des Geschäftsvorfalls bei Ihnen
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Sets a new reference
     *
     * Eindeutige Referenznummer des Geschäftsvorfalls bei Ihnen
     *
     * @param string $reference
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Gets as iJReference
     *
     * @return int
     */
    public function getIJReference()
    {
        return $this->iJReference;
    }

    /**
     * Sets a new iJReference
     *
     * @param int $iJReference
     * @return self
     */
    public function setIJReference($iJReference)
    {
        $this->iJReference = $iJReference;
        return $this;
    }

    /**
     * Gets as creditRating
     *
     * @return string
     */
    public function getCreditRating()
    {
        return $this->creditRating;
    }

    /**
     * Sets a new creditRating
     *
     * @param string $creditRating
     * @return self
     */
    public function setCreditRating($creditRating)
    {
        $this->creditRating = $creditRating;
        return $this;
    }

    /**
     * Gets as creditRatingLevel
     *
     * @return string
     */
    public function getCreditRatingLevel()
    {
        return $this->creditRatingLevel;
    }

    /**
     * Sets a new creditRatingLevel
     *
     * @param string $creditRatingLevel
     * @return self
     */
    public function setCreditRatingLevel($creditRatingLevel)
    {
        $this->creditRatingLevel = $creditRatingLevel;
        return $this;
    }

    /**
     * Gets as remainingLimit
     *
     * @return float
     */
    public function getRemainingLimit()
    {
        return $this->remainingLimit;
    }

    /**
     * Sets a new remainingLimit
     *
     * @param float $remainingLimit
     * @return self
     */
    public function setRemainingLimit($remainingLimit)
    {
        $this->remainingLimit = $remainingLimit;
        return $this;
    }

    /**
     * Gets as creditDecision
     *
     * @return string
     */
    public function getCreditDecision()
    {
        return $this->creditDecision;
    }

    /**
     * Sets a new creditDecision
     *
     * @param string $creditDecision
     * @return self
     */
    public function setCreditDecision($creditDecision)
    {
        $this->creditDecision = $creditDecision;
        return $this;
    }

    /**
     * Gets as requestStatus
     *
     * @return int
     */
    public function getRequestStatus()
    {
        return $this->requestStatus;
    }

    /**
     * Sets a new requestStatus
     *
     * @param int $requestStatus
     * @return self
     */
    public function setRequestStatus($requestStatus)
    {
        $this->requestStatus = $requestStatus;
        return $this;
    }

    /**
     * Gets as lastStatusChange
     *
     * @return \DateTime
     */
    public function getLastStatusChange()
    {
        return $this->lastStatusChange;
    }

    /**
     * Sets a new lastStatusChange
     *
     * @param \DateTime $lastStatusChange
     * @return self
     */
    public function setLastStatusChange(\DateTime $lastStatusChange)
    {
        $this->lastStatusChange = $lastStatusChange;
        return $this;
    }

    /**
     * Gets as transactionNumber
     *
     * @return string
     */
    public function getTransactionNumber()
    {
        return $this->transactionNumber;
    }

    /**
     * Sets a new transactionNumber
     *
     * @param string $transactionNumber
     * @return self
     */
    public function setTransactionNumber($transactionNumber)
    {
        $this->transactionNumber = $transactionNumber;
        return $this;
    }

    /**
     * Gets as decisionProcessNumber
     *
     * @return int
     */
    public function getDecisionProcessNumber()
    {
        return $this->decisionProcessNumber;
    }

    /**
     * Sets a new decisionProcessNumber
     *
     * @param int $decisionProcessNumber
     * @return self
     */
    public function setDecisionProcessNumber($decisionProcessNumber)
    {
        $this->decisionProcessNumber = $decisionProcessNumber;
        return $this;
    }

    /**
     * Adds as extraInfo
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType $extraInfo
     */
    public function addToExtraInfo(\Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType $extraInfo)
    {
        $this->extraInfo[] = $extraInfo;
        return $this;
    }

    /**
     * isset extraInfo
     *
     * @param int|string $index
     * @return bool
     */
    public function issetExtraInfo($index)
    {
        return isset($this->extraInfo[$index]);
    }

    /**
     * unset extraInfo
     *
     * @param int|string $index
     * @return void
     */
    public function unsetExtraInfo($index)
    {
        unset($this->extraInfo[$index]);
    }

    /**
     * Gets as extraInfo
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[]
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Sets a new extraInfo
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[] $extraInfo
     * @return self
     */
    public function setExtraInfo(array $extraInfo)
    {
        $this->extraInfo = $extraInfo;
        return $this;
    }

    /**
     * Adds as processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType $processingInfo
     */
    public function addToProcessingInfo(\Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType $processingInfo)
    {
        $this->processingInfo[] = $processingInfo;
        return $this;
    }

    /**
     * isset processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @param int|string $index
     * @return bool
     */
    public function issetProcessingInfo($index)
    {
        return isset($this->processingInfo[$index]);
    }

    /**
     * unset processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @param int|string $index
     * @return void
     */
    public function unsetProcessingInfo($index)
    {
        unset($this->processingInfo[$index]);
    }

    /**
     * Gets as processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType[]
     */
    public function getProcessingInfo()
    {
        return $this->processingInfo;
    }

    /**
     * Sets a new processingInfo
     *
     * Meldungen zum Verarbeitungsstatus (Info, Warnungen, Fehler)
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ProcessingInfoType[] $processingInfo
     * @return self
     */
    public function setProcessingInfo(array $processingInfo)
    {
        $this->processingInfo = $processingInfo;
        return $this;
    }

    /**
     * Adds as orderHistory
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\OrderHistoryType $orderHistory
     */
    public function addToOrderHistory(\Drupal\commerce_byjuno\Client\CreditDecision\OrderHistoryType $orderHistory)
    {
        $this->orderHistory[] = $orderHistory;
        return $this;
    }

    /**
     * isset orderHistory
     *
     * @param int|string $index
     * @return bool
     */
    public function issetOrderHistory($index)
    {
        return isset($this->orderHistory[$index]);
    }

    /**
     * unset orderHistory
     *
     * @param int|string $index
     * @return void
     */
    public function unsetOrderHistory($index)
    {
        unset($this->orderHistory[$index]);
    }

    /**
     * Gets as orderHistory
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\OrderHistoryType[]
     */
    public function getOrderHistory()
    {
        return $this->orderHistory;
    }

    /**
     * Sets a new orderHistory
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\OrderHistoryType[] $orderHistory
     * @return self
     */
    public function setOrderHistory(array $orderHistory)
    {
        $this->orderHistory = $orderHistory;
        return $this;
    }


}

