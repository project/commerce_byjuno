<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing ProcessingInfoType
 *
 *
 * XSD Type: ProcessingInfoType
 */
class ProcessingInfoType
{

    /**
     * @var string $code
     */
    private $code = null;

    /**
     * @var string $classification
     */
    private $classification = null;

    /**
     * @var string $description
     */
    private $description = null;

    /**
     * Gets as code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets a new code
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Gets as classification
     *
     * @return string
     */
    public function getClassification()
    {
        return $this->classification;
    }

    /**
     * Sets a new classification
     *
     * @param string $classification
     * @return self
     */
    public function setClassification($classification)
    {
        $this->classification = $classification;
        return $this;
    }

    /**
     * Gets as description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets a new description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


}

