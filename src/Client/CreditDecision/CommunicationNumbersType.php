<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing CommunicationNumbersType
 *
 *
 * XSD Type: CommunicationNumbersType
 */
class CommunicationNumbersType
{

    /**
     * @var string $telephonePrivate
     */
    private $telephonePrivate = null;

    /**
     * @var string $telephoneOffice
     */
    private $telephoneOffice = null;

    /**
     * @var string $fax
     */
    private $fax = null;

    /**
     * @var string $mobile
     */
    private $mobile = null;

    /**
     * @var string $email
     */
    private $email = null;

    /**
     * Gets as telephonePrivate
     *
     * @return string
     */
    public function getTelephonePrivate()
    {
        return $this->telephonePrivate;
    }

    /**
     * Sets a new telephonePrivate
     *
     * @param string $telephonePrivate
     * @return self
     */
    public function setTelephonePrivate($telephonePrivate)
    {
        $this->telephonePrivate = $telephonePrivate;
        return $this;
    }

    /**
     * Gets as telephoneOffice
     *
     * @return string
     */
    public function getTelephoneOffice()
    {
        return $this->telephoneOffice;
    }

    /**
     * Sets a new telephoneOffice
     *
     * @param string $telephoneOffice
     * @return self
     */
    public function setTelephoneOffice($telephoneOffice)
    {
        $this->telephoneOffice = $telephoneOffice;
        return $this;
    }

    /**
     * Gets as fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets a new fax
     *
     * @param string $fax
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Gets as mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets a new mobile
     *
     * @param string $mobile
     * @return self
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * Gets as email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets a new email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


}

