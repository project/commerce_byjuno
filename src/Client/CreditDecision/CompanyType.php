<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing CompanyType
 *
 *
 * XSD Type: CompanyType
 */
class CompanyType
{

    /**
     * @var string $companyName1
     */
    private $companyName1 = null;

    /**
     * @var string $companyName2
     */
    private $companyName2 = null;

    /**
     * @var string $language
     */
    private $language = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\AddressType $currentAddress
     */
    private $currentAddress = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType $previousAddress
     */
    private $previousAddress = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType $communicationNumbers
     */
    private $communicationNumbers = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\OrderingPersonType $orderingPerson
     */
    private $orderingPerson = null;

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[] $extraInfo
     */
    private $extraInfo = [
        
    ];

    /**
     * @var \Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType $deliveryAddress
     */
    private $deliveryAddress = null;

    /**
     * Gets as companyName1
     *
     * @return string
     */
    public function getCompanyName1()
    {
        return $this->companyName1;
    }

    /**
     * Sets a new companyName1
     *
     * @param string $companyName1
     * @return self
     */
    public function setCompanyName1($companyName1)
    {
        $this->companyName1 = $companyName1;
        return $this;
    }

    /**
     * Gets as companyName2
     *
     * @return string
     */
    public function getCompanyName2()
    {
        return $this->companyName2;
    }

    /**
     * Sets a new companyName2
     *
     * @param string $companyName2
     * @return self
     */
    public function setCompanyName2($companyName2)
    {
        $this->companyName2 = $companyName2;
        return $this;
    }

    /**
     * Gets as language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets a new language
     *
     * @param string $language
     * @return self
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * Gets as currentAddress
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\AddressType
     */
    public function getCurrentAddress()
    {
        return $this->currentAddress;
    }

    /**
     * Sets a new currentAddress
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\AddressType $currentAddress
     * @return self
     */
    public function setCurrentAddress(\Drupal\commerce_byjuno\Client\CreditDecision\AddressType $currentAddress)
    {
        $this->currentAddress = $currentAddress;
        return $this;
    }

    /**
     * Gets as previousAddress
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType
     */
    public function getPreviousAddress()
    {
        return $this->previousAddress;
    }

    /**
     * Sets a new previousAddress
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType $previousAddress
     * @return self
     */
    public function setPreviousAddress(\Drupal\commerce_byjuno\Client\CreditDecision\PreviousAddressType $previousAddress)
    {
        $this->previousAddress = $previousAddress;
        return $this;
    }

    /**
     * Gets as communicationNumbers
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType
     */
    public function getCommunicationNumbers()
    {
        return $this->communicationNumbers;
    }

    /**
     * Sets a new communicationNumbers
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType $communicationNumbers
     * @return self
     */
    public function setCommunicationNumbers(\Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType $communicationNumbers)
    {
        $this->communicationNumbers = $communicationNumbers;
        return $this;
    }

    /**
     * Gets as orderingPerson
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\OrderingPersonType
     */
    public function getOrderingPerson()
    {
        return $this->orderingPerson;
    }

    /**
     * Sets a new orderingPerson
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\OrderingPersonType $orderingPerson
     * @return self
     */
    public function setOrderingPerson(\Drupal\commerce_byjuno\Client\CreditDecision\OrderingPersonType $orderingPerson)
    {
        $this->orderingPerson = $orderingPerson;
        return $this;
    }

    /**
     * Adds as extraInfo
     *
     * @return self
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType $extraInfo
     */
    public function addToExtraInfo(\Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType $extraInfo)
    {
        $this->extraInfo[] = $extraInfo;
        return $this;
    }

    /**
     * isset extraInfo
     *
     * @param int|string $index
     * @return bool
     */
    public function issetExtraInfo($index)
    {
        return isset($this->extraInfo[$index]);
    }

    /**
     * unset extraInfo
     *
     * @param int|string $index
     * @return void
     */
    public function unsetExtraInfo($index)
    {
        unset($this->extraInfo[$index]);
    }

    /**
     * Gets as extraInfo
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[]
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Sets a new extraInfo
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType[] $extraInfo
     * @return self
     */
    public function setExtraInfo(array $extraInfo)
    {
        $this->extraInfo = $extraInfo;
        return $this;
    }

    /**
     * Gets as deliveryAddress
     *
     * @return \Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Sets a new deliveryAddress
     *
     * @param \Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType $deliveryAddress
     * @return self
     */
    public function setDeliveryAddress(\Drupal\commerce_byjuno\Client\CreditDecision\DeliveryAddressType $deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
        return $this;
    }


}

