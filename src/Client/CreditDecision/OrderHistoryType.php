<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing OrderHistoryType
 *
 *
 * XSD Type: OrderHistoryType
 */
class OrderHistoryType
{

    /**
     * @var string $customerReference
     */
    private $customerReference = null;

    /**
     * @var string $orderId
     */
    private $orderId = null;

    /**
     * @var \DateTime $lastOrderDate
     */
    private $lastOrderDate = null;

    /**
     * @var string $matchLevel
     */
    private $matchLevel = null;

    /**
     * @var float $unpaidOrderAmount
     */
    private $unpaidOrderAmount = null;

    /**
     * Gets as customerReference
     *
     * @return string
     */
    public function getCustomerReference()
    {
        return $this->customerReference;
    }

    /**
     * Sets a new customerReference
     *
     * @param string $customerReference
     * @return self
     */
    public function setCustomerReference($customerReference)
    {
        $this->customerReference = $customerReference;
        return $this;
    }

    /**
     * Gets as orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Sets a new orderId
     *
     * @param string $orderId
     * @return self
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * Gets as lastOrderDate
     *
     * @return \DateTime
     */
    public function getLastOrderDate()
    {
        return $this->lastOrderDate;
    }

    /**
     * Sets a new lastOrderDate
     *
     * @param \DateTime $lastOrderDate
     * @return self
     */
    public function setLastOrderDate(\DateTime $lastOrderDate)
    {
        $this->lastOrderDate = $lastOrderDate;
        return $this;
    }

    /**
     * Gets as matchLevel
     *
     * @return string
     */
    public function getMatchLevel()
    {
        return $this->matchLevel;
    }

    /**
     * Sets a new matchLevel
     *
     * @param string $matchLevel
     * @return self
     */
    public function setMatchLevel($matchLevel)
    {
        $this->matchLevel = $matchLevel;
        return $this;
    }

    /**
     * Gets as unpaidOrderAmount
     *
     * @return float
     */
    public function getUnpaidOrderAmount()
    {
        return $this->unpaidOrderAmount;
    }

    /**
     * Sets a new unpaidOrderAmount
     *
     * @param float $unpaidOrderAmount
     * @return self
     */
    public function setUnpaidOrderAmount($unpaidOrderAmount)
    {
        $this->unpaidOrderAmount = $unpaidOrderAmount;
        return $this;
    }


}

