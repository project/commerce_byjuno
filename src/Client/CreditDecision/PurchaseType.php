<?php

namespace Drupal\commerce_byjuno\Client\CreditDecision;

/**
 * Class representing PurchaseType
 *
 *
 * XSD Type: PurchaseType
 */
class PurchaseType
{

    /**
     * @var int $consolidatedProductRiskCategory
     */
    private $consolidatedProductRiskCategory = null;

    /**
     * @var int[] $productCategories
     */
    private $productCategories = null;

    /**
     * Gets as consolidatedProductRiskCategory
     *
     * @return int
     */
    public function getConsolidatedProductRiskCategory()
    {
        return $this->consolidatedProductRiskCategory;
    }

    /**
     * Sets a new consolidatedProductRiskCategory
     *
     * @param int $consolidatedProductRiskCategory
     * @return self
     */
    public function setConsolidatedProductRiskCategory($consolidatedProductRiskCategory)
    {
        $this->consolidatedProductRiskCategory = $consolidatedProductRiskCategory;
        return $this;
    }

    /**
     * Adds as productCategory
     *
     * @return self
     * @param int $productCategory
     */
    public function addToProductCategories($productCategory)
    {
        $this->productCategories[] = $productCategory;
        return $this;
    }

    /**
     * isset productCategories
     *
     * @param int|string $index
     * @return bool
     */
    public function issetProductCategories($index)
    {
        return isset($this->productCategories[$index]);
    }

    /**
     * unset productCategories
     *
     * @param int|string $index
     * @return void
     */
    public function unsetProductCategories($index)
    {
        unset($this->productCategories[$index]);
    }

    /**
     * Gets as productCategories
     *
     * @return int[]
     */
    public function getProductCategories()
    {
        return $this->productCategories;
    }

    /**
     * Sets a new productCategories
     *
     * @param int[] $productCategories
     * @return self
     */
    public function setProductCategories(array $productCategories)
    {
        $this->productCategories = $productCategories;
        return $this;
    }


}

