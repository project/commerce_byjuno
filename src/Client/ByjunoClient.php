<?php

namespace Drupal\commerce_byjuno\Client;

use Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType;
use Drupal\commerce_byjuno\Client\CreditDecision\AddressType;
use Drupal\commerce_byjuno\Client\CreditDecision\CommunicationNumbersType;
use Drupal\commerce_byjuno\Client\CreditDecision\Company;
use Drupal\commerce_byjuno\Client\CreditDecision\ExtraInfoTypeType;
use Drupal\commerce_byjuno\Client\CreditDecision\OrderingPersonType;
use Drupal\commerce_byjuno\Client\CreditDecision\Person;
use Drupal\commerce_byjuno\Client\CreditDecision\Request\RequestAType\CustomerAType;
use Drupal\commerce_byjuno\Client\SendTrx\Request;
use Drupal\commerce_byjuno\Client\SendTrx\Request\RequestAType\TransactionAType;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Language\LanguageManagerInterface;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\BaseTypesHandler;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\XmlSchemaDateHandler;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * ByJuno Client class.
 */
class ByjunoClient implements ByjunoClientInterface {

  /**
   * Helper class to construct a HTTP client with Drupal specific config.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Request stack that controls the lifecycle of requests.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Common interface for the language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Factory for getting extension lists by type.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  private ExtensionPathResolver $extensionPathResolver;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Http\ClientFactory $httpClientFactory
   *   Helper class to construct a HTTP client with Drupal specific config.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack that controls the lifecycle of requests.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Common interface for the language manager service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   Factory for getting extension lists by type.
   */
  public function __construct(ClientFactory $httpClientFactory, RequestStack $request_stack, LanguageManagerInterface $language_manager, ExtensionPathResolver $extensionPathResolver) {
    $this->httpClientFactory = $httpClientFactory;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->extensionPathResolver = $extensionPathResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client_factory'),
      $container->get('request_stack'),
      $container->get('language_manager'),
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function reservationRequest(OrderInterface $order, PaymentMethodInterface $payment_method) {
    $customer = $this->buildCustomer($order, $payment_method, FALSE);

    $customer_type = new CustomerAType();
    if ($customer instanceof Person) {
      $customer_type->setPerson($customer);
    }
    elseif ($customer instanceof Company) {
      $customer_type->setCompany($customer);
    }

    $customer_type->setReference($order->getCustomerId());

    $request = new CreditDecision\Request();
    $request->addToCustomer($customer_type);
    $request->setRequestId($payment_method->id());

    $payment_gateway = $payment_method->getPaymentGateway();
    $store = $order->getStore();

    return $this->creditDecisionRequest($payment_gateway, $store, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function reservationConfirmation(PaymentInterface $payment, $capture = FALSE) {
    $payment_method = $payment->getPaymentMethod();
    $order = $payment->getOrder();

    $customer = $this->buildCustomer($order, $payment_method, TRUE);

    $transaction_number = new ExtraInfoTypeType();
    $transaction_number->setName('TRANSACTIONNUMBER')
      ->setValue($payment_method->getRemoteId());
    $customer->addToExtraInfo($transaction_number);

    $order_id = new ExtraInfoTypeType();
    $order_id->setName('ORDERID')
      ->setValue($order->id());
    $customer->addToExtraInfo($order_id);

    $byjuno_payment_method = new ExtraInfoTypeType();
    $byjuno_payment_method->setName('PAYMENTMETHOD')
      ->setValue($payment_method->byjuno_payment_method->value);
    $customer->addToExtraInfo($byjuno_payment_method);

    $byjuno_payment_type = new ExtraInfoTypeType();
    $byjuno_payment_type->setName('REPAYMENTTYPE')
      ->setValue($payment_method->byjuno_payment_type->value);
    $customer->addToExtraInfo($byjuno_payment_type);

    $customer_type = new CustomerAType();
    if ($customer instanceof Person) {
      $customer_type->setPerson($customer);
    }
    elseif ($customer instanceof Company) {
      $customer_type->setCompany($customer);
    }

    $customer_type->setReference($order->getCustomerId());

    $request = new CreditDecision\Request();
    $request->addToCustomer($customer_type);
    $request->setRequestId($payment_method->id());

    $payment_gateway = $payment_method->getPaymentGateway();
    $store = $order->getStore();

    return $this->creditDecisionRequest($payment_gateway, $store, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function settlement(PaymentInterface $payment, Price $amount = NULL) {
    $payment_gateway = $payment->getPaymentGateway();
    $order = $payment->getOrder();
    $amount = $amount ?: $payment->getAmount();

    $transaction = new TransactionAType();
    $transaction->setOrderId($order->id());
    $transaction->setClientRef($order->getCustomerId());
    $transaction->setTransactionType('CHARGE');
    $transaction->setTransactionDate(new \DateTime());
    $transaction->setTransactionAmount($amount->getNumber());
    $transaction->setTransactionCurrency($amount->getCurrencyCode());
    $transaction->setAdditional1('INVOICE');
    $transaction->setAdditional2($order->getOrderNumber());

    $open_balance = $payment->getBalance()->subtract($amount)->getNumber();
    $transaction->setOpenBalance($open_balance);

    if ($amount->equals($payment->getAmount())) {
      $transaction->setPartialDelivery('N');
    }
    else {
      $transaction->setPartialDelivery('Y');
    }

    $request = new Request();
    $request->addToTransaction($transaction);
    $request->setRequestId($payment->getPaymentMethod()->id());

    return $this->sendTrxRequest($payment_gateway, $order->getStore(), $request);
  }

  /**
   * {@inheritdoc}
   */
  public function refund(PaymentInterface $payment, Price $amount = NULL) {
    $payment_gateway = $payment->getPaymentGateway();
    $order = $payment->getOrder();
    $amount = $amount ?: $payment->getAmount()->subtract($payment->getBalance());

    $transaction = new TransactionAType();
    $transaction->setOrderId($order->id());
    $transaction->setClientRef($order->getCustomerId());
    $transaction->setTransactionType('REFUND');
    $transaction->setTransactionDate(new \DateTime());
    $transaction->setTransactionAmount($amount->getNumber());
    $transaction->setTransactionCurrency($amount->getCurrencyCode());

    $open_balance = $payment->getBalance()->getNumber();
    $transaction->setOpenBalance($open_balance);

    $transaction->setAdditional1('INVOICE');
    $transaction->setAdditional2($order->getOrderNumber());

    $request = new Request();
    $request->addToTransaction($transaction);
    $request->setRequestId($payment->getPaymentMethod()->id());

    return $this->sendTrxRequest($payment_gateway, $order->getStore(), $request);
  }

  /**
   * {@inheritdoc}
   */
  public function cancellation(PaymentInterface $payment, Price $amount = NULL) {
    $payment_gateway = $payment->getPaymentGateway();
    $order = $payment->getOrder();
    $amount = $amount ?: $payment->getBalance();

    $transaction = new TransactionAType();
    $transaction->setOrderId($order->id());
    $transaction->setClientRef($order->getCustomerId());
    $transaction->setTransactionType('CANCEL');
    $transaction->setTransactionDate(new \DateTime());
    $transaction->setTransactionAmount($amount->getNumber());
    $transaction->setTransactionCurrency($amount->getCurrencyCode());

    $transaction->setAdditional1('INVOICE');
    $transaction->setAdditional2($order->getOrderNumber());

    $open_balance = $payment->getBalance()->subtract($amount)->getNumber();
    $transaction->setOpenBalance($open_balance);

    $request = new Request();
    $request->addToTransaction($transaction);
    $request->setRequestId($payment->getPaymentMethod()->id());

    return $this->sendTrxRequest($payment_gateway, $order->getStore(), $request);
  }

  /**
   * Request a Decision concerning the credit.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   Defines the interface for payment gateway configuration entities.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   Defines the interface for stores.
   * @param \Drupal\commerce_byjuno\Client\CreditDecision\Request $request
   *   Request to be sent from client to Intrum Justitia.
   *
   * @return \Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType
   *   Class representing ResponseAType
   */
  private function creditDecisionRequest(PaymentGatewayInterface $payment_gateway, StoreInterface $store, CreditDecision\Request $request) {
    $gateway_configuration = $payment_gateway->getPluginConfiguration();

    $request->setClientId($gateway_configuration['client_id']);
    $request->setVersion('1.0');
    $request->setEmail($store->getEmail());
    $request->setUserID($gateway_configuration['user_id']);
    $request->setPassword($gateway_configuration['user_pw']);

    $serializer = $this->getSerializer();
    $request_xml = $serializer->serialize($request, 'xml');

    if ($payment_gateway->getPluginConfiguration()['mode'] == 'test') {
      $render_xml = str_replace('&', '&amp;', $request_xml);
      $render_xml = str_replace('<', '&lt;', $render_xml);
      $render_xml = str_replace('>', '&gt;', $render_xml);
      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . $render_xml . '</code></pre>');
    }

    $uri_path = 'response.cfm';

    $response_xml = $this->postRequest($payment_gateway, $request_xml, $uri_path);

    /** @var \Drupal\commerce_byjuno\Client\CreditDecision\Response\ResponseAType $response */
    $response = $serializer->deserialize($response_xml, 'Drupal\commerce_byjuno\Client\CreditDecision\Response', 'xml');
    assert($response instanceof ResponseAType);
    $processing_info = $response->getProcessingInfo()[0];
    if ($processing_info->getClassification() == 'ERR') {
      throw new InvalidRequestException($processing_info->getDescription());
    }
    return $response;
  }

  /**
   * Send the TRX request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   Defines the interface for payment gateway configuration entities.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   Defines the interface for stores.
   * @param \Drupal\commerce_byjuno\Client\SendTrx\Request $request
   *   Request to be sent from client to Intrum Justitia.
   *
   * @return \Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType
   *   Class representing ResponseAType
   */
  private function sendTrxRequest(PaymentGatewayInterface $payment_gateway, StoreInterface $store, SendTrx\Request $request) {
    $gateway_configuration = $payment_gateway->getPluginConfiguration();

    $request->setClientId($gateway_configuration['client_id']);
    $request->setVersion('1.41');
    $request->setEmail($store->getEmail());
    $request->setUserID($gateway_configuration['user_id']);
    $request->setPassword($gateway_configuration['user_pw']);

    $serializer = $this->getSerializer();
    $request_xml = $serializer->serialize($request, 'xml');

    if ($payment_gateway->getPluginConfiguration()['mode'] == 'test') {
      $render_xml = str_replace('&', '&amp;', $request_xml);
      $render_xml = str_replace('<', '&lt;', $render_xml);
      $render_xml = str_replace('>', '&gt;', $render_xml);

      \Drupal::logger('commerce_byjuno')->info('<pre><code>' . $render_xml . '</code></pre>');
    }

    $uri_path = 'sendTransaction.cfm';

    $response_xml = $this->postRequest($payment_gateway, $request_xml, $uri_path);
    $response = $serializer->deserialize($response_xml, 'Drupal\commerce_byjuno\Client\SendTrx\Response', 'xml');
    assert($response instanceof \Drupal\commerce_byjuno\Client\SendTrx\Response\ResponseAType);
    $processing_info = $response->getTransaction()->getProcessingInfo();
    if ($processing_info->getClassification() == 'ERR') {
      throw new InvalidRequestException($processing_info->getDescription());
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  private function postRequest(PaymentGatewayInterface $payment_gateway, $xml, $uri_path) {
    $gateway_configuration = $payment_gateway->getPluginConfiguration();

    $form_params = [
      'REQUEST' => $xml,
    ];

    if ($gateway_configuration['mode'] == 'live') {
      $uri = "https://ws.cdp.intrum.com/services/creditCheckDACH_01_41/$uri_path";
    }
    else {
      $uri = "https://ws.cdp.intrum.com/services/creditCheckDACH_01_41_TEST/$uri_path";
    }

    $httpClient = $this->httpClientFactory->fromOptions();
    $response = $httpClient->post(
      $uri,
      [
        'form_params' => $form_params,
        'auth' => $this->buildBasicAuth($payment_gateway),
      ]
    );

    return $response->getBody();
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   * @param bool $closed
   *
   * @return \Drupal\commerce_byjuno\Client\CreditDecision\Company|\Drupal\commerce_byjuno\Client\CreditDecision\Person
   * @throws \Exception
   */
  protected function buildCustomer(OrderInterface $order, PaymentMethodInterface $payment_method, $closed = FALSE) {
    $billing_profile = $payment_method->getBillingProfile();

    $order_closed = new ExtraInfoTypeType();
    $order_closed->setName('ORDERCLOSED');
    if ($closed) {
      $order_closed->setValue('YES');
    }
    else {
      $order_closed->setValue('NO');
    }

    if ($payment_method->byjuno_customer_type->value == 'person') {
      $customer = new Person();
      $customer->addToExtraInfo($order_closed);
      $customer->setDateOfBirth(new \DateTime($payment_method->byjuno_date_of_birth->value));
      $customer->setGender($payment_method->byjuno_gender->value);
      $customer->setFirstName($billing_profile->address->given_name);
      $customer->setLastName($billing_profile->address->family_name);
    }
    elseif ($payment_method->byjuno_customer_type->value == 'company') {
      $customer = new Company();
      $customer->addToExtraInfo($order_closed);

      if ($billing_profile->address->organisation == '') {
        $customer->setCompanyName1($billing_profile->address->organization);
      }
      else {
        throw new InvalidRequestException($this->t('Organization name is required for Companies'));
      }

      $person = new Person();
      $person->setGender($payment_method->byjuno_gender->value);
      $person->setFirstName($billing_profile->address->given_name);
      $person->setLastName($billing_profile->address->family_name);
      $ordering_person = new OrderingPersonType();
      $ordering_person->setPerson($person);
      $customer->setOrderingPerson($ordering_person);

      $register_number = new ExtraInfoTypeType();
      $register_number->setName('REGISTERNUMBER');
      $register_number->setValue($payment_method->byjuno_uid->value);
      $customer->addToExtraInfo($register_number);
    }
    else {
      throw new PaymentGatewayException();
    }

    // Address.
    $current_address = new AddressType();
    $current_address->setFirstLine($billing_profile->address->address_line1 . ' ' . $billing_profile->address->address_line2)
      ->setPostCode($billing_profile->address->postal_code)
      ->setTown($billing_profile->address->locality)
      ->setCountryCode($billing_profile->address->country_code);
    $customer->setCurrentAddress($current_address);

    // Communication Numbers.
    $communication_numbers = new CommunicationNumbersType();
    $communication_numbers->setEmail($order->getEmail());

    // Phone number on billing profile.
    if ($billing_profile->hasField('field_phonenumber') &&
      !$billing_profile->get('field_phonenumber')->isEmpty()) {
      $communication_numbers->setTelephonePrivate($billing_profile->field_phonenumber->value);
    }

    $customer->setCommunicationNumbers($communication_numbers);

    /*
     * Order Information
     */

    // Total.
    $order_amount = new ExtraInfoTypeType();
    $order_amount->setName('ORDERAMOUNT')
      ->setValue($order->getTotalPrice()->getNumber());
    $customer->addToExtraInfo($order_amount);

    // Currency.
    $order_currency = new ExtraInfoTypeType();
    $order_currency->setName('ORDERCURRENCY')
      ->setValue($order->getTotalPrice()->getCurrencyCode());
    $customer->addToExtraInfo($order_currency);

    // IP Address.
    $ip = new ExtraInfoTypeType();
    $ip->setName('IP')
      ->setValue($this->requestStack->getCurrentRequest()->getClientIp());
    $customer->addToExtraInfo($ip);

    // Fingerprint.
    $device_fingerprint_id = new ExtraInfoTypeType();
    $device_fingerprint_id->setName('DEVICE_FINGERPRINT_ID')
      ->setValue($order->uuid());
    $customer->addToExtraInfo($device_fingerprint_id);

    // @todo When language is added to order update. See #2603482
    $current_language_code = $this->languageManager->getCurrentLanguage()->getId();
    $current_language_code = $current_language_code == 'und' ? 'en' : $current_language_code;
    $customer->setLanguage($current_language_code);

    /*
     * Shipping
     */

    $profiles = $order->collectProfiles();
    if (isset($profiles['shipping'])) {
      if ($profiles['shipping']->address->given_name) {
        $delivery_first_name = new ExtraInfoTypeType();
        $delivery_first_name->setName('DELIVERY_FIRSTNAME')
          ->setValue($profiles['shipping']->address->given_name);
        $customer->addToExtraInfo($delivery_first_name);
      }

      if ($profiles['shipping']->address->family_name) {
        $delivery_last_name = new ExtraInfoTypeType();
        $delivery_last_name->setName('DELIVERY_LASTNAME')
          ->setValue($profiles['shipping']->address->family_name);
        $customer->addToExtraInfo($delivery_last_name);
      }

      if ($profiles['shipping']->address->address_line1) {
        $delivery_first_line = new ExtraInfoTypeType();
        $delivery_first_line->setName('DELIVERY_FIRSTLINE')
          ->setValue($profiles['shipping']->address->address_line1 . ' ' . $profiles['shipping']->address->address_line2);
        $customer->addToExtraInfo($delivery_first_line);
      }

      if ($profiles['shipping']->address->organization) {
        $delivery_company_name = new ExtraInfoTypeType();
        $delivery_company_name->setName('DELIVERY_COMPANYNAME')
          ->setValue($profiles['shipping']->address->organization);
        $customer->addToExtraInfo($delivery_company_name);
      }

      if ($profiles['shipping']->address->locality) {
        $delivery_town = new ExtraInfoTypeType();
        $delivery_town->setName('DELIVERY_TOWN')
          ->setValue($profiles['shipping']->address->locality);
        $customer->addToExtraInfo($delivery_town);
      }

      if ($profiles['shipping']->address->postal_code) {
        $delivery_postcode = new ExtraInfoTypeType();
        $delivery_postcode->setName('DELIVERY_POSTCODE')
          ->setValue($profiles['shipping']->address->postal_code);
        $customer->addToExtraInfo($delivery_postcode);
      }

      if ($profiles['shipping']->address->country_code) {
        $delivery_countrycode = new ExtraInfoTypeType();
        $delivery_countrycode->setName('DELIVERY_COUNTRYCODE')
          ->setValue($profiles['shipping']->address->country_code);
        $customer->addToExtraInfo($delivery_countrycode);
      }

      // Phone number.
      if ($profiles['shipping']->hasField('field_phonenumber') &&
        !$profiles['shipping']->get('field_phonenumber')->isEmpty()) {
        $communication_numbers->setTelephonePrivate($profiles['shipping']->field_phonenumber->value);
      }
    }

    // Set communication details.
    $customer->setCommunicationNumbers($communication_numbers);

    return $customer;
  }

  /**
   * Initialize and return the Serializer.
   *
   * @return \JMS\Serializer\Serializer
   */
  private function getSerializer() {

    $serializerBuilder = SerializerBuilder::create();
    $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
      $serializerBuilder->addDefaultHandlers();
      // XMLSchema List handling.
      $handler->registerSubscribingHandler(new BaseTypesHandler());
      // XMLSchema date handling.
      $handler->registerSubscribingHandler(new XmlSchemaDateHandler());
    });

    $module_dir = $this->extensionPathResolver->getPath('module', 'commerce_byjuno');
    $serializerBuilder->addMetadataDir("$module_dir/metadata/creditdecision", 'Drupal\commerce_byjuno\Client\CreditDecision');
    $serializerBuilder->addMetadataDir("$module_dir/metadata/sendtrx", 'Drupal\commerce_byjuno\Client\SendTrx');

    return $serializerBuilder->build();
  }

  /**
   * Add the BasicAuth config to the Payment Gateway.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   Defines the interface for payment gateway configuration entities.
   *
   * @return array
   */
  private function buildBasicAuth(PaymentGatewayInterface $payment_gateway) {
    $gateway_configuration = $payment_gateway->getPluginConfiguration();
    return [
      $gateway_configuration['user_id'],
      $gateway_configuration['user_pw'],
    ];
  }

}
